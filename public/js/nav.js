var navButton = document.querySelector('.header--nav-toggle');
var navMenu = document.querySelector('nav');
var win = window;

function openMenu(event) {
    navButton.classList.toggle('active');
    navMenu.classList.toggle('active');

    event.preventDefault();
    event.stopImmediatePropagation();
}

function closeMenu(event) {
    if (navButton.classList.contains('active')) {
        navButton.classList.remove('active');
        navMenu.classList.remove('active');
    }
}
navButton.addEventListener('click', openMenu, false);

win.addEventListener('click',closeMenu, false);